%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define YELLOW  "\x1b[33m"
#define COLOR_RESET   "\x1b[0m"

int yylex();
void yyerror(const char *str);

extern int lines;
int nlist;
int nid;

%}

%start commands

%token NEWLINE PROGRAM ID SEMICOL END VAR INT REAL PROCEDURE ELSE READLN WRITELN IF WHILE DO PERIOD COMMA COLON OPAR CPAR NUM_INT NUM_REAL REL DIFF ATTR SIGN OP_ADD OP_MULT MYBEGIN BUFF THEN REPEAT UNTIL

%%

commands: 
    | 
    commands command;
    ;

    command:
        program
        |begin
        |end
        |procedure
        |else
        |readln
        |writeln
        |while
        |if
        |operation
        |var
        |repeat
        |until
        ;

        program:
            PROGRAM ID SEMICOL {}
            |PROGRAM error SEMICOL {yyclearin; yyerror("Nome do programa");}
            |PROGRAM ID error {yyclearin; yyerror(";");}
            |error SEMICOL
            |error endLine
            ;

        begin:
            MYBEGIN{}
            ;

        end:
            END PERIOD {}
            |END SEMICOL {}
            |END error {yyclearin; yyerror(". ou ;");}
            ;

        procedure:
            PROCEDURE ID OPAR idList COLON numList CPAR SEMICOL { if(nid != nlist) {yyclearin; yyerror("Diferente");} nid = 0; nlist = 0; }
            |PROCEDURE error OPAR idList COLON numList CPAR SEMICOL { yyclearin; yyerror("Identificador"); nid = 0; nlist = 0; }
            |PROCEDURE ID error idList COLON numList CPAR SEMICOL { yyclearin; yyerror("("); nid = 0; nlist = 0; }
            |PROCEDURE ID OPAR error COLON numList CPAR SEMICOL { yyclearin; yyerror("Lista de identificadores"); nid = 0; nlist = 0; }
            |PROCEDURE ID OPAR idList error numList CPAR SEMICOL { yyclearin; yyerror(":"); nid = 0; nlist = 0; }
            |PROCEDURE ID OPAR idList COLON error CPAR SEMICOL { yyclearin; yyerror("Lista de tipos"); nid = 0; nlist = 0; }
            |PROCEDURE ID OPAR idList COLON numList error SEMICOL { yyclearin; yyerror(")"); nid = 0; nlist = 0; }
            |PROCEDURE ID OPAR idList COLON numList CPAR error { yyclearin; yyerror(";"); nid = 0; nlist = 0; }
            ;

        var:
            VAR idList COLON INT SEMICOL { nid = 0; }
            |VAR idList COLON REAL SEMICOL { nid = 0; }
            |VAR error COLON INT SEMICOL { yyclearin; yyerror("Lista de identificadores"); nid = 0; }
            |VAR error COLON REAL SEMICOL { yyclearin; yyerror("Lista de identificadores"); nid = 0; }
            |VAR idList error INT SEMICOL { yyclearin; yyerror(":"); nid = 0; }
            |VAR idList error REAL SEMICOL { yyclearin; yyerror(":"); nid = 0; }
            |VAR idList COLON error SEMICOL { yyclearin; yyerror("Tipo"); nid = 0; }
            |VAR idList COLON REAL error { yyclearin; yyerror(";"); nid = 0; }
            |VAR idList COLON INT error { yyclearin; yyerror(";"); nid = 0; }
            ;

        else:
            ELSE {}
            ;

        readln:
            READLN OPAR idList CPAR SEMICOL{ nid = 0; }
            |READLN error idList CPAR SEMICOL{yyclearin; yyerror("("); nid = 0; }
            |READLN OPAR error CPAR SEMICOL{yyclearin; yyerror("Lista de identificadores"); nid = 0; }
            |READLN OPAR idList error SEMICOL{yyclearin; yyerror(")"); nid = 0; }
            |READLN OPAR idList CPAR error{yyclearin; yyerror(";"); nid = 0; }
            ;

        writeln:
            WRITELN OPAR ID CPAR SEMICOL{}
            |WRITELN error ID CPAR SEMICOL{yyclearin; yyerror("(");}
            |WRITELN OPAR error CPAR SEMICOL{yyclearin; yyerror("( Espera-se um simbolo )");}
            |WRITELN OPAR ID error SEMICOL{yyclearin; yyerror(")");}
            |WRITELN OPAR ID CPAR error {yyclearin; yyerror(";");}
            ;
        
        while:
            WHILE statement compare statement DO {}
            |WHILE error compare statement DO {yyclearin; yyerror("Expressao");}
            |WHILE statement compare error DO {yyclearin; yyerror("Expressao");}
            |WHILE statement error statement DO {yyclearin; yyerror("Comparacao");}
            |WHILE statement compare statement error {yyclearin; yyerror("comando do");}
            ;

        repeat:
            REPEAT {}
            ;
        
        until:
            UNTIL statement compare statement {}
            |UNTIL error compare statement { yyclearin; yyerror("Expressao"); }
            |UNTIL statement error statement { yyclearin; yyerror("Comparacao"); }
            |UNTIL statement compare error { yyclearin; yyerror("Expressao"); }
            ;

        if:
            IF statement compare statement THEN {}
            |IF error compare statement THEN {yyclearin; yyerror("Expressao");}
            |IF statement compare error THEN {yyclearin; yyerror("Expressao ola2");}
            |IF statement error statement THEN {yyclearin; yyerror("Comparacao");}
            |IF statement compare statement error {yyclearin; yyerror("Comando then");}
            ;

        operation:
            ID ATTR statement SEMICOL {}
            |error ATTR statement SEMICOL {yyclearin; yyerror("Identificador");}
            |ID error statement SEMICOL {yyclearin; yyerror("Comparacao");}
            |ID ATTR error SEMICOL {yyclearin; yyerror("Atribuicao");}
            |ID ATTR statement error {yyclearin; yyerror(";");}
            ;

        expression:
            number OP_ADD number {}
            |number OP_MULT number {}
            |expression OP_ADD number {}
            |expression OP_MULT number {}
            |number OP_ADD error {yyclearin; yyerror("Valor");}
            |number OP_MULT error {yyclearin; yyerror("Valor");}
            ;
        
        statement:
            number {}
            | expression {}
            ;

        number:
            ID {}
            |NUM_INT {}
            |NUM_REAL {}
            ;

        idList:
            ID { nid++; }
            |idList COMMA ID { nid++; };
            ;
        
        numList:
            REAL { nlist++; }
            |INT { nlist++; }
            |numList COMMA REAL { nlist++; }
            |numList COMMA INT { nlist++; }
            ;
        
        compare:
            REL
            |DIFF
            ;
        
        endLine:
            NEWLINE
            |endLine SEMICOL
            ;
%%

int main() {
    lines = 0;
    nlist = 0;
    nid = 0;
    return(yyparse());
}

void yyerror(const char *str) {
    if(strcmp(str,"syntax error")){
            if(!strcmp(str, "Diferente")) {
                printf (YELLOW "Linha %d. Erro Sintatico. Numero diferente de identificadores e tipos atribuidos\n" COLOR_RESET, lines+1);    
            } else {     
                printf (YELLOW "Linha %d. Erro Sintatico. Esperando %s\n" COLOR_RESET, (strcmp(str,";"))? lines + 1 : lines, str);
            }
    }
}

int yywrap() {
    return 1;
}