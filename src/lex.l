%{
	#include <stdio.h>
	#include <string.h>
	#include "yacc.tab.h"

	#define RED     "\x1b[31m"
	#define COLOR_RESET   "\x1b[0m"

	int lines;
%}

%%

program {
	return PROGRAM;
}

begin {
	return MYBEGIN;
}

end {
	return END;
}

var {
	return VAR;
}

integer {
	return INT;
}

real {
	return REAL;
}

procedure {
	return PROCEDURE;
}

else {
	return ELSE;
}

readln {
	return READLN;
}

writeln {
	return WRITELN;
}

if {
	return IF;
}

then {
	return THEN;
}

while {
	return WHILE;
}

do {
	return DO;
}

repeat {
	return REPEAT;
}

until {
	return UNTIL;
}

\. {
	return PERIOD;
}

, {
	return COMMA;
}

; {
	return SEMICOL;
}

: {
	return COLON;
}

\( {
	return OPAR;
}

\) {
	return CPAR;
}

[0-9]([0-9])* {
	return NUM_INT;
}

([0-9]+)[\.]([0-9]+) {
	return NUM_REAL;
}


[a-zA-Z]([a-zA-Z]|[0-9]|[-]|[_])* {
	return ID;
}

[<][>] {
	return DIFF;
}

[<|>|=|>=|<=] {
	return REL;
}

[:][=] {
	return ATTR;
}

[:][=]([\t\n]+)[+|-] {
	return SIGN;
}

[+|-] { 
	return OP_ADD;
}

[*|/] {
	return OP_MULT;
}

\{(.)*[\}]* {
	/*Ignora comentários*/;
	int strl;
	int i, j;

	strl = strlen(yytext);
	j = 0;
	for(i = 0; i < strl; i++) {
		if(yytext[i] == '}') {
			j = 1;
		}
	}

	if(!j) {
		printf(RED "%s - Linha %d. Erro Lexico: Comentario nao finalizado na mesma linha\n" COLOR_RESET, yytext, lines+1);
	}
}

[ \t]+ /*Ignora espaços em branco*/;

\n { 
	lines++; /*Ignora as quebras de linha*/
	return NEWLINE;
};
	
[.]+ {
	/*Trata qualquer caracter que não esteja na linguagem como erro*/;
	printf(RED "%s - Linha %d. Erro Lexico: Caracter nao reconhecido\n" COLOR_RESET, yytext, lines+1);
}

([\#-\&]|[\?]|[\@])+ { /*Reconhecimento dos caracteres especiais*/
	printf(RED "%s - Linha %d. Erro Lexico: Caracter nao reconhecido\n" COLOR_RESET, yytext, lines+1);
}

([0-9]+)([a-zA-Z])*([0-9])* {
	/*Trata o erro de uma letra dentro de um número*/;
	printf(RED "%s - Linha %d. Erro Lexico: Numero mal formado\n" COLOR_RESET, yytext, lines+1);
}

%%