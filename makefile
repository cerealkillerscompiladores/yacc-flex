#Compiladores utilizados
CC = gcc
FCC = flex
YCC = yacc

#Flags necessárias para compilação
FLAGS = -d

#Diretórios do programa
SDIR = ./src

#Arquivo executavel
EXEC = analisador.exe

#Arquivo fonte gerado
LEX = $(SDIR)/lex.yy.c
YACC = $(SDIR)/yacc.tab.c
all: $(EXEC)

$(YACC): $(SDIR)/yacc.y
	$(YCC) $(FLAGS) $(SDIR)/yacc.y -o $@ 

$(LEX): $(SDIR)/lex.l
	$(FCC) -o $@ $(SDIR)/lex.l

$(EXEC): $(YACC) $(LEX)
	$(CC) $(YACC) $(LEX) -o $@

run:
	./$(EXEC)

.PHONY: clean
clean:
	rm $(LEX)
	rm $(YACC)
	rm $(SDIR)/yacc.tab.h
	rm $(EXEC)